-- 1. Specifier le mode CSV et le séparateur à utiliser avant l'import SQLite
.mode csv
.separator ,

-- 2. Importer le fichier access.log dans la table "access_log"
.import access_log.csv raw_data

-- CREATE TABLE access_log_mini(host TEXT, ident TEXT, authuser TEXT, date1 TEXT, date2 TEXT, request TEXT, status TEXT, bytes TEXT, 1 TEXT, 2 TEXT, 3 TEXT) from access_log;
--.quit


