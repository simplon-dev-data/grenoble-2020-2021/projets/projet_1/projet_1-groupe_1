#!/bin/bash

# Ce script automatise le pipeline de données pour traitement log serveur Web
# les logs sont au format CLF
# Utilisation:
#   ./pipeline.sh

# variable URL téléchargement fichier log
DATA_URL="https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J"

# variable fichier log zippé
DATA_FILE_ZIP="access.log.zip"

# variable fichier log dezippé
DATA_FILE_UNZIP="access.log"

# variable fichier log contenant les 10000 premières lignes
DATA_FILE="access_log_10000.log"

# variable fichier CSV contenant les 10000 premières lignes
DATA_FILE_CSV="access_log_10000.csv"

# variable fichier log dezippé complet non utilisé pour les tests
#DATA_FILE="access.log"

# variable du 1er fichier de log ayant subi REGEX "s/\,/\;/g"
DATA_FILE1="access_1.log"

# variable du 2ème fichier de log ayant subi REGEX "s/\"/\ /g"
DATA_FILE2="access_2.log"

# variable du 3ème fichier de log ayant subi REGEX "s/ * /,/g"
DATA_FILE3="access_3.log"

# variable du CSV definitif comportant le dataset prêt a être importé en DB
DATA_FILE_CSV="access_5.csv"

# variable du fichier de DB
DATABASE_FILE="access_log.db"

# variable du CSV comportant la colonne date1 extrait de la DB 
DATA_TIME="date_heure.csv"

# variable du 1er fichier CSV ayant subi REGEX '1d'
DATA_TIME1="date_heure1.csv"

# variable du 2ème fichier CSV ayant subi REGEX -i "1i date heure"
DATA_TIME2="date_heure2.csv"

# variable du 3ème fichier CSV ayant subi REGEX "s/\:/ /"
DATA_TIME3="date_heure3.csv"



##############################################################################
# Partie 1 : extraction de données
##############################################################################

# Test de la présence du fichier Zippé si non présent téléchargement, puis unzip sinon => partie 2
if [ -f $DATA_FILE_ZIP ]; then
   echo -e '\E[37' "$DATA_FILE_ZIP existe => étape 2 Transformation des données avant import"
else
   echo -e '\E[31;40m'"$DATA_FILE_ZIP n'est pas present"
   echo -e '\E[31;40m'"$DATA_FILE_ZIP Téléchargement en cours"
   wget -O ${DATA_FILE_ZIP} -q ${DATA_URL}
   unzip $DATA_FILE_ZIP

fi

# extraction des 10000 premières lignes du fichier access.log pour Dataset test
head -10000 $DATA_FILE_UNZIP >$DATA_FILE

echo "extraction des données $DATA_FILE OK"
##############################################################################
# Partie 2 : transformation des données avant import en base
##############################################################################

# ajout de la première ligne de colonne avec les en-têtes de colonne
sed -i "1i host ident authuser date1 date2 http request shit status bytes url navigateur os appareil" $DATA_FILE
cat $DATA_FILE | sed "s/\,/\;/g" >$DATA_FILE1
cat $DATA_FILE1 | sed "s/\"/\ /g" >$DATA_FILE2
cat $DATA_FILE2 | sed "s/ * /,/g" >$DATA_FILE3
cat $DATA_FILE3 >$DATA_FILE_CSV

echo "transformation des données avant import en base $DATA_FILE_CSV OK"
##############################################################################
# Partie 3 : chargement de données en base
##############################################################################

sqlite3 ${DATABASE_FILE} < load1.sql
echo "Chargement des données en base ${DATABASE_FILE} (load.sql)"

##############################################################################
# Partie 4 : transformation de données aprés import
##############################################################################

sqlite3 ${DATABASE_FILE} < transform.sql
echo "Transformation des données en base ${DATABASE_FILE} (transform.sql)"

sed '1d' $DATA_TIME >$DATA_TIME1
sed -i "1i date heure minute" $DATA_TIME1
cat $DATA_TIME1 | sed "s/\:/,/g" >$DATA_TIME2
cat $DATA_TIME2 | sed "s/ * /,/g" >$DATA_TIME3

echo "Transformation des données en base ${DATA_TIME_CSV}"
##############################################################################
# # Partie 5 : Import CSV Date_Heure
##############################################################################

sqlite3 ${DATABASE_FILE} < load2.sql
echo "Export des données en CSV (Import Date Heure)"
